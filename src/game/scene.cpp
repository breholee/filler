/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "scene.hpp"

#include "block.hpp"
#include "defs.hpp"
#include "logger.hpp"

#include <algorithm>
#include <cassert>

const Color BORDER_COLOR = { 0.3, 0.3, 0.4 };

const float INITIAL_FALL_SPEED = 1000.0;
const unsigned int ANIM_SPEED = 50;
const double BOARD_SPEED = 0.2;

static Logger logger(__FILE__);

bool are_equal(double a, double b) { return std::fabs(a - b) < 0.0001; }

/**
 * Constructor.
 *
 * Create table with borders and first shape.
 */
Scene::Scene() :
	game_state(GS_START)
{
	reset();
}

/**
 * Reset
 */
void Scene::reset()
{
	current_command = CMD_NONE;
	time = 0;
	last_animation_change = 0;
	fall_speed = INITIAL_FALL_SPEED;
	completed_lines = 0;
	front_side = true;
	score = 0;
	stored = 0;
	level = 1;
}

/**
 * Retrieve all the blocks to be displayed.
 *
 * That's all the blocks from the table, as well as the ones
 * from the current shape, if there is one.
 */
std::vector<Block> Scene::get_blocks() const
{
	std::vector<Block> blocks;

	for (auto &block : table)
		blocks.push_back(block);

	for (auto &shape : shapes) {
		for (auto block : shape.blocks) {
			block.x += shape.x;
			block.y += shape.y;
			block.z = shape.z;
			blocks.push_back(block);
		}
	}

	return blocks;
}

/**
 * Add a block to the table.
 *
 * Nothing done if there is already a block at that location.
 */
void Scene::add_block(int x, int y, int z, Color color, BlockStyle style)
{
	if (is_location_empty(x, y, z))
		table.push_back({ x, y, z, color, style });
}

/**
 * Check whether a location is occupied.
 */
bool Scene::is_location_empty(int x, int y, int z) const
{
	if (x < 0 || x >= table_width)
		return false;

	if (y < 0 || y >= table_height)
		return false;

	if (z < 0 || z > 1)
		return false;

	for (auto &block : table)
		if (block.x == x && block.y == y && block.z == z)
			return false;

	return true;
}

/**
 * Update contents of scene after time advance
 */
void Scene::update(gametime dt)
{
	if (current_command == CMD_PAUSE)
		return;

	switch (game_state) {
	case GS_START:
		update_starting(dt);
		break;
	case GS_PLAY:
	case GS_PLAY_NEXT:
		update_playing(dt);
		break;
	case GS_TURN:
		update_rotating(dt);
		break;
	case GS_END:
		update_ending(dt);
		break;
	default: assert(!"Unhandled game state");
	}
}

/**
 * Update contents of scene while playings
 */
void Scene::update_playing(gametime dt)
{
	assert(shapes.size() <= 1);

	rotate_board(dt);

	int left = front_side ? -1 : 1;

	auto it = std::begin(shapes);

	while (it != std::end(shapes)) {
		auto &shape = *it;
		shape.time += dt;

		bool go_down = false; // shape to be moved down?
		bool store = false;   // landing? then store blocks.

		// Movement by user
		switch (current_command) {
		case CMD_NONE: break;
		case CMD_LEFT: attempt_move(shape, left, 0); break;
		case CMD_RIGHT: attempt_move(shape, -left, 0); break;
		case CMD_DOWN:
			if (attempt_move(shape, 0, -1))
				shape.last_fall = shape.time; // time reset
			else
				store = true;
			break;
		case CMD_DROP: go_down = true; break;
		case CMD_ROTATE_LEFT: attempt_rotation(shape, front_side); break;
		default: assert(!"Unhandled user command in Scene");
		}

		// Clear command except for drop
		if (current_command != CMD_DROP)
			current_command = CMD_NONE;

		// Falling?
		if (shape.time >= shape.last_fall + fall_speed) {
			shape.last_fall += fall_speed;
			go_down = true;
		}

		// Going down
		if (go_down) {
			if (!attempt_move(shape, 0, -1)) {
				stored++ ;
				store = true;
			}
		}

		// Landed: store blocks in table and remove shape
		if (store) {
			current_command = CMD_NONE;
			store_shape(shape);
			score += shape.blocks.size();
			it = shapes.erase(it);
			continue; // iterator reset
		}
		it++;
	}

	remove_completed_lines();

	if (stored % 10 == 9) {
		stored++;
		switch_state(GS_TURN);
	}

	// Recreate a shape if needed
	if (shapes.empty())
		add_shape();
}

/**
 * Checks whether a shape can move into another location
 */
bool Scene::may_move(const Shape &shape, int dx, int dy) const
{
	for (auto &block : shape.blocks) {
		auto x = shape.x + block.x + dx;
		auto y = shape.y + block.y + dy;

		if (!is_location_empty(x, y, shape.z))
		    return false;
	}

	return true;
}

/**
 * Add a shape at the top of the table.
 */
void Scene::add_shape()
{
	assert(shapes.empty());

	Shape shape = retrieve_shape();
	shape.x = find_horizontal_location(shape);
	shape.y = table_height - 1 - shape.top_offset();
	shape.z = front_side ? 1 : 0;
	shape.time = 0;
	shape.last_fall = 0;
	shapes.push_back(shape);

	if (!may_move(shape, 0, 0)) {
		store_shape(shape);
		shapes.clear();
		switch_state(GS_END);
	}
}

/**
 * Store a shape into the table.
 *
 * Put all the blocks of a shape into the table. (after landing).
 */
void Scene::store_shape(const Shape &shape)
{
	for (auto block : shape.blocks) {
		auto x = shape.x + block.x;
		auto y = shape.y + block.y;
		add_block(x, y, shape.z, block.color, block.style );
	}
}

/**
 * Receive request from user
 */
void Scene::receive_command(UserCommand command)
{
	if (current_command == CMD_PAUSE) {
		current_command = CMD_NONE;
		return;
	}

	if (current_command == CMD_DROP)
		return;

	current_command = command;
}

/**
 * Rotate shape counterclockwise if possible
 *
 * @return true if successful
 */
bool Scene::attempt_rotation(Shape &shape, bool rotate_left) const
{
	if (!shape.may_rotate)
		return false;

	Shape new_shape = shape;
	new_shape.blocks.clear();

	for (auto block : shape.blocks) {
		Block new_block = block;
		if (rotate_left) {
			new_block.y = block.x;
			new_block.x = block.y * -1;
		} else {
			new_block.x = block.y;
			new_block.y = block.x * -1;
		}
		new_block.z = shape.z;
		new_shape.blocks.push_back(new_block);
	}

	// Rotation successful
	if (may_move(new_shape, 0, 0)) {
		shape = new_shape;
		return true;
	}

	// Move one step on left side if it helps rotating
	if (attempt_move(new_shape, -1, 0)) {
		shape = new_shape;
		return true;
	}

	// Otherwise same thing on right side
	if (attempt_move(new_shape, 1, 0)) {
		shape = new_shape;
		return true;
	}

	return false;
}

/**
 * Move shape if possible
 *
 * \return true if successful
 */
bool Scene::attempt_move(Shape &shape, int dx, int dy) const
{
	if (!may_move(shape, dx, dy))
		return false;

	shape.x += dx;
	shape.y += dy;
	return true;
}

/**
 * Remove completed lines.
 */
void Scene::remove_completed_lines()
{
	// Find the number of blocks in each line
	std::vector<int> block_numbers(table_height);
	for (auto &block : table)
		block_numbers[block.y]++;

	// Exclude the lower line
	block_numbers.erase(block_numbers.begin());

	// Create the list of completed lines by height
	std::vector<int> completed;
	for (unsigned int i = 0; i < block_numbers.size(); ++i)
		if (block_numbers[i] == table_width * 2) // filled in both sides
			completed.push_back(i + 1);

	// Then from top to bottom
	std::reverse(completed.begin(), completed.end());
	score += completed.size();

	// For each completed line
	for (auto i : completed) {
		complete_line();
		auto it = begin(table);
		while (it != end(table)) {
			if (it->y == i) {
				// Remove the blocks of the line
				it = table.erase(it);
			} else {
				// And move down those that are above
				if (it->y > i)
					it->y--;
				it++;
			}
		}
		// Recreate border at the top
		int h = table_height - 1;
		for (auto depth = 0; depth < 2; depth++) {
			int x = table_width - 1;
			add_block(0, h, depth, BORDER_COLOR, BS_TRIANGLE);
			add_block(x, h, depth, BORDER_COLOR, BS_TRIANGLE);
		}
	}
}

/**
 * Transition to another game state (play, pause, game over, etc.)
 */
void Scene::switch_state(GameState state)
{
	current_command = CMD_NONE;
	time = 0;
	last_animation_change = 0;

	switch (state) {
	case GS_PLAY:
		reset();
		logger.info("Level %d", level);
		break;
	case GS_TURN:
		front_side = !front_side;
		board_target = front_side ? 0.0 : 180.0;
		break;
	case GS_END:
		queue = std::queue<Shape>();
		board_target = 0.0;
	default: break;
	}
	game_state = state;
}

/**
 * Update contents of scene during "game over"
 */
void Scene::update_ending(gametime dt)
{
	rotate_board(dt);

	time += dt;
	if (time < last_animation_change + ANIM_SPEED)
		return;

	last_animation_change += ANIM_SPEED;

	// Remove 10 blocks randomly
	for (int i = 0; i < 10 && !table.empty(); ++i) {
		int k = rand() % table.size();
		table.erase(std::begin(table) + k);
	}

	// When done, restart
	if (table.empty()) {
		logger.info("Score: %d", score);
		switch_state(GS_START);
	}
}

/**
 * Update contents of scene during start of game
 */
void Scene::update_starting(gametime dt)
{
	rotate_board(dt);

	time += dt;
	if (time < last_animation_change + ANIM_SPEED)
		return;

	last_animation_change += ANIM_SPEED;

	// Fill lower row
	if (is_location_empty(0, 0, 0)) {
		for (int x = 0; x < table_width; x++)
			for (int i = 0; i < 2; i++)
				add_block(x, 0, i, BORDER_COLOR, BS_TRIANGLE);
	} else {
		// Add border blocks above the previous ones
		int y = 0;
		while (y < table_height && !is_location_empty(0, y, 0))
			y++;

		if (y < table_height) {
			for (int i = 0; i < 2; i++) {
				int x = table_width - 1;
				add_block(0, y, i, BORDER_COLOR, BS_TRIANGLE);
				add_block(x, y, i, BORDER_COLOR, BS_TRIANGLE);
			}
		}
	}

	// When done, switch to "play" mode
	if (!is_location_empty(0, table_height - 1, 0))
		switch_state(GS_PLAY);
}

/**
 * Complete line
 */
void Scene::complete_line()
{
	completed_lines++;

	if (completed_lines % 5 == 0) {
		fall_speed = fall_speed * 0.9;
		level++;
		logger.info("Level %d", level);
		logger.debug("  speed %f", fall_speed);
	}
}

/**
 * Retrieve next shape.
 *
 * Refill queue if empty
 */
Shape Scene::retrieve_shape()
{
	if (queue.empty())
		push_shuffled_shapes(queue);

	assert(queue.size() > 0);

	Shape shape = queue.front();
	queue.pop();
	shape.z = 1;

	if (queue.empty())
		push_shuffled_shapes(queue);

	return shape;
}

/**
 * Get next shapes.
 *
 * Currently provides either 0 or 1 next shape.
 */
std::vector<Shape> Scene::get_next_shapes() const
{
	std::vector<Shape> next_shapes;

	if (!queue.empty()) {
		Shape shape = queue.front();
		shape.z = front_side ? 1 : 0;
		next_shapes.push_back(shape);
	}
	return next_shapes;
}

/**
 * Find a location where to put the next shape
 */
int Scene::find_horizontal_location(const Shape &source_shape) const
{
	auto shape = source_shape;
	int result = table_width / 2; // default

	// maximal range
	int x_min = 1 - shape.left_offset() * -1;
	int x_max = table_width - 2 - shape.right_offset();

	bool found = false;
	int r_min, r_max;

	// Find left-most available spot
	int x = x_min;
	while (x <= x_max) {
		shape.x = x;
		if (may_move(shape, 0, 0)) {
			found = true;
			break;
		}
		x++;
	}

	if (!found)
		return result;

	// Look to the right until location is unavailable
	r_min = x;
	found = false;
	while (x <= x_max) {
		shape.x = x;
		if (!may_move(shape, 0, 0)) {
			found = true;
			break;
		}
		x++;
	}
	r_max = x - 1;

	return (r_min + r_max) / 2;
}

/**
 * Get board current orientation
 */
double Scene::get_board_angle() const
{
	return board_angle;
}

/**
 * Apply board rotation with current rotation speed.
 *
 * Rotation stops after one full rotation.
 */
void Scene::rotate_board(gametime dt)
{
	if (are_equal(board_angle, board_target)) {
		board_angle = board_target;
		return;
	}

	bool was_below = board_angle < board_target;
	board_angle += BOARD_SPEED * dt;

	if (board_angle > 360.0 || are_equal(board_angle, 360.0))
		board_angle = 0;

	if (are_equal(board_angle, board_target)) {
		board_angle = board_target;
		return;
	}

	if (was_below && board_angle >= board_target)
		board_angle = board_target;
}

/**
 * Update contents of scene during board rotation
 */
void Scene::update_rotating(gametime dt)
{
	rotate_board(dt);

	if (are_equal(board_target, board_angle))
		switch_state(GS_PLAY_NEXT);
}
