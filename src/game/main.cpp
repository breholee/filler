/*
 * Diskenn
 * Copyright © 2018, 2020, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defs.hpp"
#include "error.hpp"
#include "logger.hpp"
#include "scene.hpp"
#include "view.hpp"

#include <SDL2/SDL.h>

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <stdexcept>

static void initialize_sdl();
static void run_event_loop();
static UserCommand read_keyboard_event(const SDL_Event &);
static void handle_window_event(const SDL_Event &, View &);

static bool sdl_initialized = false;
static Logger logger(__FILE__);

/*
 * Entry point
 */
int main()
{
	Logger::set_global_level(Logger::TRACE);

	try {
		initialize_sdl();
		run_event_loop();
	}
	catch (const std::exception &e) {
		logger.error("%s", e.what());
	}

	if (sdl_initialized) {
		SDL_Delay(100);
		SDL_Quit();
	}
	return EXIT_SUCCESS;
}

/**
 * Initialize SDL
 */
void initialize_sdl()
{
	logger.debug(__func__);

	int status = SDL_Init(SDL_INIT_VIDEO);

	if (status < 0)
		throw sdl_exception("SDL initialization failed.");

	sdl_initialized = true;
}

/*
 * Main event loop
 */
void run_event_loop()
{
	auto last_tick = SDL_GetTicks();

	Scene scene;
	View view(scene);

	view.initialize();

	bool exit_loop = false;

	while (!exit_loop) {

		auto tick = SDL_GetTicks();

		while (tick < last_tick + 10)
			tick = SDL_GetTicks();

		SDL_Event event;
		UserCommand command = CMD_NONE;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				command = CMD_EXIT;
				break;
			case SDL_WINDOWEVENT:
				handle_window_event(event, view);
				break;
			case SDL_KEYDOWN:
				command = read_keyboard_event(event);
				break;
			default: break;
			}
		}

		switch (command) {
		case CMD_EXIT:
			exit_loop = true;
			break;
		case CMD_FULLSCREEN:
			view.toggle_fullscreen();
			break;
		case CMD_LEFT:
		case CMD_RIGHT:
		case CMD_DOWN:
		case CMD_DROP:
		case CMD_ROTATE_LEFT:
		case CMD_PAUSE:
			scene.receive_command(command);
			break;
		case CMD_NONE:
		default: break;
		}

		auto dt = tick - last_tick;

		scene.update(dt);
		view.render();
		view.swap_window();
		last_tick = tick;
	}
}

/*
 * Read keyboard input and convert it into a user command
 */
UserCommand read_keyboard_event(const SDL_Event &event)
{
	UserCommand command = CMD_NONE;

	if (event.key.repeat == 1)
		return command;

	switch (event.key.keysym.sym)
	{
	case SDLK_ESCAPE:
	case SDLK_q:
		command = CMD_EXIT;
		break;
	case SDLK_f:
		command = CMD_FULLSCREEN;
		break;
	case SDLK_LEFT:
	case SDLK_KP_4:
	case SDLK_h:
		command = CMD_LEFT;
		break;
	case SDLK_RIGHT:
	case SDLK_KP_6:
	case SDLK_l:
		command = CMD_RIGHT;
		break;
	case SDLK_DOWN:
	case SDLK_KP_2:
	case SDLK_j:
		command = CMD_DOWN;
		break;
	case SDLK_UP:
	case SDLK_KP_5:
	case SDLK_k:
		command = CMD_ROTATE_LEFT;
		break;
	case SDLK_SPACE:
	case SDLK_KP_8:
		command = CMD_DROP;
		break;
	case SDLK_p:
		command = CMD_PAUSE;
		break;
	default: break;
	}

	return command;
}

/**
 * Handle window-related event (resize)
 */
void handle_window_event(const SDL_Event &event, View &view)
{
	assert(event.type == SDL_WINDOWEVENT);

	switch (event.window.event) {
	case SDL_WINDOWEVENT_RESIZED:
	case SDL_WINDOWEVENT_SIZE_CHANGED:
		view.change_size(event.window.data1, event.window.data2);
		break;
	default: break;
	}
}
