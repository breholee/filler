/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCENE_H
#define SCENE_H

#include "defs.hpp"
#include "shape.hpp"

#include <queue>
#include <vector>

class Scene
{
public:
	Scene();

	void receive_command(UserCommand command);
	void update(gametime);

	std::vector<Block> get_blocks() const;
	std::vector<Shape> get_next_shapes() const;
	double get_board_angle() const;
	double get_board_width() const;

private:
	void add_block(int x, int y, int z, Color c, BlockStyle style);
	void add_shape();
	bool attempt_move(Shape &shape, int dx, int dy) const;
	bool attempt_rotation(Shape &shape, bool rotate_left) const;
	void complete_line();
	void create_borders();
	int find_horizontal_location(const Shape &shape) const;
	bool is_location_empty(int x, int y, int z) const;
	bool may_move(const Shape &shape, int dx, int dy) const;
	void remove_completed_lines();
	void reset();
	Shape retrieve_shape();
	void rotate_board(gametime dt);
	void store_shape(const Shape &shape);
	void switch_state(GameState state);
	void update_ending(gametime dt);
	void update_playing(gametime dt);
	void update_starting(gametime dt);
	void update_rotating(gametime dt);

	int table_width = 13;
	int table_height = 26;

	std::vector<Block> table;
	std::vector<Shape> shapes;
	std::queue<Shape> queue;
	bool front_side = true;

	UserCommand current_command;
	GameState game_state;
	gametime time;
	gametime last_animation_change;
	double fall_speed;
	int completed_lines;
	int score;
	int stored;
	int level;
	double board_angle = 0.0;
	double board_target = 0.0;
};

#endif
