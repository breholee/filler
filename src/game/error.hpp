/*
 * Diskenn
 * Copyright © 2018 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define ERROR_H

#include <stdexcept>

/**
 * Exception for libsdl2 errors
 */
class sdl_exception : public std::runtime_error
{
public:
	explicit sdl_exception(const std::string &message);
	virtual ~sdl_exception() noexcept;

	virtual const char *what() const noexcept override;
};

#endif
