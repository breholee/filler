# Diskenn
# Copyright © 2018, 2020, 2021 Benoît Bréholée
#
# SPDX-License-Identifier: GPL-3.0+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.13)

project(Diskenn VERSION 0.1)
set(VERSION_STATUS "ds")

find_package(SDL2 REQUIRED COMPONENTS SDL2::SDL2)
find_package(OpenGL REQUIRED)

set(CMAKE_CXX_STANDARD 17)

set(DISKENN_BIN "diskenn")
set(DISKENN_SRC src/game/main.cpp src/game/view.cpp src/game/error.cpp
  src/game/logger.cpp src/game/scene.cpp src/game/shape.cpp)

configure_file(src/game/config.h.in ${CMAKE_BINARY_DIR}/gen/config.h )
include_directories(${CMAKE_BINARY_DIR}/gen)

add_definitions(-Wall -Wextra -pedantic -fmax-errors=10 -Wfloat-equal
-Wswitch-default -Wredundant-decls -Wmissing-declarations -Wshadow
-Wzero-as-null-pointer-constant -Wuseless-cast -Wdelete-incomplete
-Wlogical-op -Wlogical-not-parentheses -Wdate-time -Wduplicated-cond
-Wsuggest-override -Wsuggest-final-types -Wsuggest-final-methods
-Wdouble-promotion)

include_directories(src/include)
include_directories(SYSTEM ${SDL2_INCLUDE_DIRS} ${OPENGL_INCLUDE_DIRS})

add_executable(${DISKENN_BIN} ${DISKENN_SRC})

target_link_libraries(${DISKENN_BIN} ${SDL2_LIBRARIES} ${OPENGL_LIBRARIES})

install(TARGETS ${DISKENN_BIN} DESTINATION bin)
